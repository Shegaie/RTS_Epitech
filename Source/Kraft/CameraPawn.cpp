// Fill out your copyright notice in the Description page of Project Settings.

#include "CameraPawn.h"

#include "GameFramework/SpringArmComponent.h"

// Sets default values
ACameraPawn::ACameraPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootScene = CreateDefaultSubobject<USceneComponent>(TEXT("RootScene"));
	RootComponent = RootScene;

	Margin = 30;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(RootScene);
	SpringArm->bDoCollisionTest = false;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(SpringArm);
}

// Called when the game starts or when spawned
void ACameraPawn::BeginPlay()
{
	Super::BeginPlay();
	PC = Cast<APlayerController>(GetController());
	PC->GetViewportSize(ScreenSizeX, ScreenSizeY);
	RootComponent->SetWorldLocation(FVector(-1300, 0, 800));
	SpringArm->SetWorldRotation(FRotator(-50, 0, 0));
}

// Called every frame
void ACameraPawn::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
	MoveCamera(GetCameraPanDirection());
}

// Called to bind functionality to input
void ACameraPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

FVector ACameraPawn::GetCameraPanDirection() const
{
	float MousePosX;
	float MousePosY;
	float CamDirectionX = 0;
	float CamDirectionY = 0;

	PC->GetMousePosition(MousePosX, MousePosY);
	if (MousePosX <= Margin) {
		CamDirectionY = -1;
	}
	if (MousePosY <= Margin) {
		CamDirectionX = 1;
	}
	if (MousePosX >= ScreenSizeX - Margin) {
		CamDirectionY = 1;
	}
	if (MousePosY >= ScreenSizeY - Margin) {
		CamDirectionX = -1;
	}
	return FVector(CamDirectionX, CamDirectionY, 0);
}

void ACameraPawn::MoveCamera(const FVector Dir)
{
	if (Dir != FVector::ZeroVector) {
		const float CamSpeed = 15;
		AddActorWorldOffset(Dir * (CamSpeed + ZoomStep * 2));
	}
}

void ACameraPawn::ZoomCamera(int32 Direction)
{
	const FRotator SpringArmRotation = SpringArm->GetRelativeRotation();
	const FVector RootComponentLocation = RootComponent->GetRelativeLocation();

	UE_LOG(LogTemp, Warning, TEXT("Rotation Pitch: %f | Direction: %i | ZoomStep: %i"), SpringArmRotation.Pitch, Direction, ZoomStep);
	if ((ZoomStep + 1 < 5 && Direction == 1) ||
		(ZoomStep > 1 && Direction == -1)) {
		ZoomStep += Direction;
		Direction *= 8;
		SpringArm->SetRelativeRotation(FRotator(SpringArmRotation.Pitch + Direction, SpringArmRotation.Yaw, SpringArmRotation.Roll));
		RootComponent->SetRelativeLocation(RootComponentLocation + FVector(0, 0, - Direction * 10));
	}
}
