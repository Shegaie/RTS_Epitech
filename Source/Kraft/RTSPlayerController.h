// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "CameraPawn.h"
#include "GameFramework/PlayerController.h"
#include "RTSHud.h"
#include "DrawDebugHelpers.h"
#include "NavigationSystem.h" 
#include "RTSPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class KRAFT_API ARTSPlayerController : public APlayerController
{
    GENERATED_BODY()

public:
    ARTSPlayerController();
    virtual void BeginPlay() override;
    virtual void SetupInputComponent() override;

    ARTSHud *HudPtr;
    ACameraPawn *CameraPtr;

    UPROPERTY(EditDefaultsOnly, Category="Spawning")
    TSubclassOf<AActor> ActorToSpawn;

protected:

    void SelectionPressed();
    void SelectionReleased();
    void MoveReleased();
    void SpawnPlayer();
    DECLARE_DELEGATE_OneParam(FDelegateOneInt, int32);
    void MouseWheelScrollEventHandler(int32 WheelDirection);
    
    TArray<AKraftCharacter *> SelectedActors;
};
