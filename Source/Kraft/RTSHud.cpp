// Fill out your copyright notice in the Description page of Project Settings.


#include "RTSHud.h"

void ARTSHud::DrawHUD()
{
    if (bStartSelecting) {
        if (FoundActors.Num() > 0) {
            for (int32 i = 0; i < FoundActors.Num(); i++) {
                FoundActors[i]->SetUnselected();
            }
        }
        FoundActors.Empty();
        
        CurrentPoint = GetMousePos2d();
        DrawRect(FLinearColor(0, 0, 1, 0.15),
                 InitialPoint.X, InitialPoint.Y, CurrentPoint.X - InitialPoint.X, CurrentPoint.Y - InitialPoint.Y);
        GetActorsInSelectionRectangle<AKraftCharacter>(InitialPoint, CurrentPoint, FoundActors, false, false);
        if (FoundActors.Num() > 0) {
            for (int32 i = 0; i < FoundActors.Num(); i++) {
                FoundActors[i]->SetSelected();
            }
        }
    }
}

FVector2D ARTSHud::GetMousePos2d()
{
    float posX;
    float posY;

    GetOwningPlayerController()->GetMousePosition(posX, posY);
    return FVector2D(posX, posY);
}
