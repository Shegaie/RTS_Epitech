// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "KraftCharacter.h"
#include "RTSHud.generated.h"

/**
 * 
 */
UCLASS()
class KRAFT_API ARTSHud : public AHUD
{
	GENERATED_BODY()

public:
    virtual void DrawHUD() override;

    
    FVector2D InitialPoint;
    FVector2D CurrentPoint;

   FVector2D GetMousePos2d();

    bool bStartSelecting = false;

    TArray<AKraftCharacter *> FoundActors;
};
