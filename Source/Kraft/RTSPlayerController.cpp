// Fill out your copyright notice in the Description page of Project Settings.


#include "RTSPlayerController.h"

#include <string>


#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "UObject/ObjectMacros.h"

ARTSPlayerController::ARTSPlayerController()
{
    bShowMouseCursor = true;
    DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ARTSPlayerController::BeginPlay()
{
    HudPtr = Cast<ARTSHud>(GetHUD());
    CameraPtr = Cast<ACameraPawn>(GetPawn());
}

void ARTSPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();
    
    InputComponent->BindAction("LeftMouseClick", IE_Pressed, this, &ARTSPlayerController::SelectionPressed);
    InputComponent->BindAction("LeftMouseClick", IE_Released, this, &ARTSPlayerController::SelectionReleased);

    InputComponent->BindAction("RightMouseClick", IE_Released, this, &ARTSPlayerController::MoveReleased);

    InputComponent->BindAction("MiddleMouseClick", IE_Released, this, &ARTSPlayerController::SpawnPlayer);

    InputComponent->BindAction<FDelegateOneInt>("MouseWheelUp", IE_Released, this, &ARTSPlayerController::MouseWheelScrollEventHandler, 1);
    InputComponent->BindAction<FDelegateOneInt>("MouseWheelDown", IE_Released, this, &ARTSPlayerController::MouseWheelScrollEventHandler, -1);
}

void ARTSPlayerController::SpawnPlayer()
{
    
    FHitResult Hit;
    GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, false, Hit);

    FActorSpawnParameters SpawnParams;
    AActor* SpawnedActor = GetWorld()->SpawnActor<AActor>(ActorToSpawn, Hit.Location, FRotator(0, 0, 0), SpawnParams);
}

void ARTSPlayerController::MouseWheelScrollEventHandler(int32 WheelDirection)
{
    CameraPtr->ZoomCamera(WheelDirection);
}


void ARTSPlayerController::SelectionPressed()
{
    HudPtr->InitialPoint = HudPtr->GetMousePos2d();
    HudPtr->bStartSelecting = true;
}

void ARTSPlayerController::SelectionReleased()
{
    HudPtr->bStartSelecting = false;
    SelectedActors = HudPtr->FoundActors;
}

void ARTSPlayerController::MoveReleased()
{
    if (SelectedActors.Num() > 0) {

        for (int32 i = 0; i < SelectedActors.Num(); i++) {
            FHitResult Hit;
            GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, false, Hit);
            FVector MoveLocation = Hit.Location;
            //FVector MoveLocation = Hit.Location + FVector(i / 2 * 100, i % 2 * 100, 0);
            UAIBlueprintHelperLibrary::SimpleMoveToLocation(SelectedActors[i]->GetController(), MoveLocation);
        }
    }
}
